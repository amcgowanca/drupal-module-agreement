<?php
/**
 * @file
 * Administration UI related functions and handlers, for example forms.
 */

/**
 * Menu callback for displaying the list page of all agreements.
 *
 * @return array
 *   Returns a renderable form of type `agreement_admin_list_agreements_form`.
 */
function agreement_admin_list_agreements_page() {
  return drupal_get_form('agreement_admin_list_agreements_form');
}

/**
 * Form handler for displaying and binding actions to a list of agreements.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 * 
 * @return array
 *   Returns a renderable form array.
 */
function agreement_admin_list_agreements_form($form, &$form_state) {
  $form = array();
  $form['actions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Actions'),
    '#collapsed' => FALSE,
    '#collapsible' => FALSE,
  );
  $form['actions']['update_options'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['actions']['update_options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Update Options'),
    '#options' => array(
      0 => t('Select an option'),
      'delete' => t('Delete selected agreements'),
    ),
    '#default_value' => 0,
  );
  $form['actions']['update_options']['update'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#validate' => array('agreement_admin_list_agreements_form_validate'),
    '#submit' => array('agreement_admin_list_agreements_form_submit'),
  );
  $form['actions']['add'] = array(
    '#type' => 'link',
    '#title' => t('Add new agreement'),
    '#href' => 'admin/structure/agreement/agreement/add',
  );

  $table_header = array(
    'title' => array('data' => t('Title'), 'a.agreement_id'),
    'operations' => array('data' => t('Operations')),
  );

  $query = db_select('agreement', 'a')->extend('PagerDefault');
  $results = $query->fields('a', array('agreement_id'))
    ->limit(50)
    ->execute();
  $agreement_ids = array();
  while ($row = $results->fetchObject()) {
    $agreement_ids[] = (int) $row->agreement_id;
  }

  $agreements = agreement_agreement_entity_load_multiple($agreement_ids);

  $table_options = array();
  if (0 < count($agreements)) {
    foreach ($agreements as $agreement) {
      $table_options[$agreement->agreement_id] = array(
        'title' => array(
          'data' => array(
            '#type' => 'link',
            '#title' => $agreement->title,
            '#href' => 'admin/structure/agreement/agreement/' . $agreement->agreement_id . '/edit',
          ),
        ),
        'operations' => array(
          'data' => array(
            '#theme' => 'links__node_operations',
            '#links' => array(
              'edit' => array(
                'title' => t('Edit'),
                'href' => 'admin/structure/agreement/agreement/' . $agreement->agreement_id . '/edit',
              ),
              'delete' => array(
                'title' => t('Delete'),
                'href' => 'admin/structure/agreement/agreement/' . $agreement->agreement_id . '/delete',
              ),
            ),
            '#attributes' => array('class' => array('links', 'inline')),
          ),
        ),
      );
    }
  }

  $form['agreements'] = array(
    '#type' => 'tableselect',
    '#header' => $table_header,
    '#options' => $table_options,
    '#empty' => t('There are currently no agreements. Please begin by adding one.'),
  );

  $form['pager'] = array('#markup' => theme('pager'));

  return $form;
}

/**
 * Validation handler for the agreement list form.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 */
function agreement_admin_list_agreements_form_validate($form, &$form_state) {
  if (!$form_state['values']['operation']) {
    form_set_error('actions][update_options][operation', t('You must select a validate update operation.'));
    return;
  }

  $agreements = array();
  if (!empty($form_state['values']['agreements'])) {
    foreach ($form_state['values']['agreements'] as $id) {
      if (!empty($id)) {
        $agreements[] = $id;
      }
    }
  }

  if (0 == count($agreements)) {
    form_set_error('actions][update_options][operation', t('At least one agreement must be selected.'));
  }
}

/**
 * Submit handler for the agreement list form.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 */
function agreement_admin_list_agreements_form_submit($form, $form_state) {
  switch ($form_state['values']['operation']) {
    case 'delete':
      $agreement_ids = array();
      foreach ($form_state['values']['agreements'] as $id) {
        if (!empty($id)) {
          $agreement_ids[] = $id;
        }
      }

      agreement_agreement_entity_delete_multiple($agreement_ids);
      drupal_set_message(t('You successfully deleted all selected agreements.'));
      break;
  }
}

/**
 * Menu callback for adding a new agreement.
 *
 * @return array
 *   Returns a renderable form of type `agreement_admin_agreement_form`.
 */
function agreement_admin_agreement_add() {
  $agreement = AgreementEntityController::defaultEntity();
  return drupal_get_form('agreement_admin_agreement_form', $agreement);
}

/**
 * Menu callback for editing a single agreement.
 *
 * @param object $agreement
 *   The agreement entity object in question to edit.
 *
 * @return array
 *   Returns a renderable form of type `agreement_admin_agreement_form`.
 */
function agreement_admin_agreement_edit($agreement) {
  drupal_set_title(t('Edit Agreement - !title', array(
    '!title' => $agreement->title,
  )));
  return drupal_get_form('agreement_admin_agreement_form', $agreement);
}

/**
 * Add or edit agreement form handler.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 * @param object $agreement
 *   An agreement entity object.
 *
 * @return array
 *   Returns a renderable form array.
 */
function agreement_admin_agreement_form($form, &$form_state, $agreement) {
  if (!isset($form_state['agreement'])) {
    if (!isset($agreement->title)) {
      $agreement->title = NULL;
    }

    $form_state['agreement'] = $agreement;
  }
  else {
    $agreement = $form_state['agreement'];
  }

  $form = array();

  // Basic agreement information.
  foreach (array('agreement_id', 'bundle_type', 'created', 'updated') as $key) {
    $form[$key] = array(
      '#type' => 'value',
      '#value' => isset($agreement->$key) ? $agreement->$key : NULL,
    );
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($form_state['values']['title']) ? $form_state['values']['title'] : $agreement->title,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );

  $form['frequency'] = array(
    '#type' => 'select',
    '#title' => t('Frequency'),
    '#description' => t('The frequency of how often a user is required to accept this agreement.'),
    '#options' => agreement_agreement_frequency_options(),
    '#default_value' => isset($form_state['values']['frequency']) ? $form_state['values']['frequency'] : $agreement->frequency,
    '#required' => TRUE,
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility Options'),
    '#collapsible' => TRUE,
    '#weight' => 100,
  );
  $form['options']['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Agreement acceptance is required for these paths'),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is wildcard. <em><front></em> is the front page."),
    '#default_value' => isset($form_state['values']['paths']) ? $form_state['values']['paths'] : (is_array($agreement->paths) ? implode("\n", $agreement->paths) : $agreement->paths),
    '#required' => TRUE,
  );
  $form['options']['roles_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Agreement acceptance is required for these roles'),
    '#description' => t('By not selecting any roles, all users will be required to accept the agreement.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $user_roles = user_roles();
  $form['options']['roles_wrapper']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User Roles'),
    '#options' => $user_roles,
    '#default_value' => isset($form_state['values']['roles']) ? $form_state['values']['roles'] : (is_array($agreement->roles) ? $agreement->roles : array()),
  );

  $form['actions'] = array('#type' => 'actions', '#weight' => 101);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 0,
    '#submit' => array('agreement_admin_agreement_form_submit'),
  );

  $form['#validate'][] = 'agreement_admin_agreement_form_validate';

  field_attach_form('agreement_entity', $agreement, $form, $form_state, entity_language('agreement_entity', $agreement));

  return $form;
}

/**
 * Validation handler for the agreement add/edit form.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 */
function agreement_admin_agreement_form_validate($form, $form_state) {
  // Void nothing here.
}

/**
 * Submit handler for the agreement add/edit form.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 */
function agreement_admin_agreement_form_submit($form, &$form_state) {
  $agreement = $form_state['agreement'];
  entity_form_submit_build_entity('agreement_entity', $agreement, $form, $form_state);

  // Normalize the paths field data.
  $paths = $agreement->paths;
  if (!empty($paths)) {
    $paths = explode("\n", $paths);
    $agreement->paths = $paths;
  }
  else {
    $agreement->paths = array();
  }

  // We want only "true" values from the roles.
  $submitted_roles = $agreement->roles;
  if (!empty($submitted_roles)) {
    $roles = array();
    foreach ($submitted_roles as $role) {
      if ($role) {
        $roles[$role] = $role;
      }
    }

    $agreement->roles = $roles;
  }
  else {
    $agreement->roles = array();
  }

  // Time save...
  $saved  = agreement_agreement_entity_save($agreement);

  if (FALSE === $saved) {
    drupal_set_message(t('An error occurred while saving this agreement. Please try again.'), 'error');
    return;
  }

  drupal_set_message(t('You have successfully saved the agreement titled "!title".', array(
    '!title' => $agreement->title,
  )));
  $form_state['redirect'] = 'admin/structure/agreement/list';
}

/**
 * Menu callback for the delete agreement page.
 *
 * @param object $agreement
 *   The agreement entity object to confirm deletion for.
 *
 * @return array
 *   The renderable drupal form.
 */
function agreement_admin_agreement_delete($agreement) {
  drupal_set_title(t('Delete Agreement - !title', array(
    '!title' => $agreement->title,
  )));
  return drupal_get_form('agreement_admin_agreement_delete_confirm_form', $agreement);
}

/**
 * Delete agreement confirmation form handler.
 *
 * @param array $form
 *   The form array of elements and data.
 * @param array $form_state
 *   The current form state data.
 * @param object $agreement
 *   An instance of the agreement entity to confirm deletion for.
 *
 * @return array
 *   Returns the renderable form array for the confirm_form().
 */
function agreement_admin_agreement_delete_confirm_form($form, $form_state, $agreement) {
  $form['#agreement'] = $agreement;
  $form['agreement_id'] = array(
    '#type' => 'value',
    '#value' => $agreement->agreement_id,
  );

  return confirm_form($form, t('Are you sure you want to delete %title?', array('%title' => $agreement->title)), 'admin/structure/agreement', t('This action cannot be undone'), t('Delete'), t('Cancel'));
}

/**
 * Submit handler for the agreement_admin_agreement_delete_confirm_form.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 */
function agreement_admin_agreement_delete_confirm_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    agreement_agreement_entity_delete($form_state['values']['agreement_id']);
    drupal_set_message(t('You have successfully deleted the agreement.'));
  }

  $form_state['redirect'] = 'admin/structure/agreement';
}
