<?php
/**
 * @file
 * The user facing, non-administration, ui related functions.
 */

/**
 * Menu callback for the agreement/* pages.
 *
 * Renders and presents to the user the agreement acceptance form.
 *
 * @param object $agreement
 *   The agreement entity object for this page/request.
 */
function agreement_page($agreement) {
  drupal_set_title($agreement->title);

  // If the `agreement_info` property does not exist within the session,
  // redirect the user to the front.
  if (!isset($_SESSION['agreement_info'])) {
    drupal_goto('<front>');
    exit;
  }

  $agreement_status = isset($_SESSION['agreement_info']->agreement_status) ? $_SESSION['agreement_info']->agreement_status : 0;
  return drupal_get_form('agreement_acceptance_form', $agreement, $agreement_status);
}

/**
 * Returns a FAPI renderable array for the user agreement acceptance form.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 * @param object $agreement
 *   The agreement entity that the user must accept.
 * @param int $status
 *   The current status of the user's acceptance.
 *
 * @return array
 *   A renderable $form array.
 */
function agreement_acceptance_form($form, &$form_state, $agreement, $status = 0) {
  global $user;

  $form = array();
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $user->uid,
  );

  $form_state['agreement'] = $agreement;

  // Attach the agreement_entity fields that have been added to display.
  $view_fields = field_attach_view('agreement_entity', $agreement, 'full');
  if (!empty($view_fields)) {
    $form['viewable'] = array(
      '#type' => 'container',
      '#weight' => 1,
    );
    $form['viewable'] = $view_fields;
  }

  // The actions used for enabling the user to accept this agreement.
  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array('container-inline'),
    ),
    '#weight' => 10,
  );
  $form['actions']['agreed'] = array(
    '#type' => 'checkbox',
    '#title' => t('I agree'),
    '#default_value' => $status,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Agreement acceptance form validate handler.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 */
function agreement_acceptance_form_validate($form, &$form_state) {
  if (!$form_state['values']['agreed']) {
    form_set_error('actions][agreed', t('You must accept the agreement prior to continuing.'));
  }
}

/**
 * Agreement acceptance form submit handler.
 *
 * @param array $form
 *   The form array of form elements and data.
 * @param array $form_state
 *   The current form state upon submission. Passed by reference.
 */
function agreement_acceptance_form_submit($form, &$form_state) {
  $agreement = $form_state['agreement'];

  $uid = $form_state['values']['uid'];
  $agreed = $form_state['values']['agreed'];

  $query = db_select('agreement_user_status', 'aus');
  $results = $query->fields('aus')
        ->condition('aus.agreement_id', $agreement->agreement_id, '=')
        ->condition('aus.uid', $uid, '=')
        ->execute();
  $agreement_user_status = $results->fetchObject();
  if (!$agreement_user_status) {
    $agreement_user_status = (object) array(
      'uid' => $uid,
      'agreement_id' => $form_state['agreement']->agreement_id,
      'status' => AGREEMENT_ACCEPTANCE_STATUS_AGREED,
      'created' => REQUEST_TIME,
      'updated' => REQUEST_TIME,
    );
    drupal_write_record('agreement_user_status', $agreement_user_status);
  }
  else {
    $agreement_user_status->updated = REQUEST_TIME;
    drupal_write_record('agreement_user_status', $agreement_user_status, 'id');
  }

  $agreement_success_destination = isset($_SESSION['agreement_info']->destination) ? $_SESSION['agreement_info']->destination : NULL;
  if (!empty($agreement_success_destination)) {
    $redirect = $agreement_success_destination;
  }
  else {
    $redirect = '<front>';
  }

  unset($_SESSION['agreement_info']);
  $form_state['redirect'] = $redirect;
}
