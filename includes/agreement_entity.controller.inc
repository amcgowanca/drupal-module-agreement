<?php
/**
 * @file
 * Agreement entity controller class.
 */

class AgreementEntityController extends DrupalDefaultEntityController implements EntityAPIControllerInterface {
  /**
   * TODO: Complete docblock.
   */
  public static function defaultEntity($type = 'agreement_path_bundle') {
    return (object) array(
      'agreement_id' => NULL,
      'created' => 0,
      'updated' => 0,
      'bundle_type' => $type,
      'title' => '',
      'paths' => array(),
      'roles' => array(),
      'frequency' => agreement_agreement_frequency_default_option(),
    );
  }

  /**
   * TODO: Complete docblock.
   */
  public function create(array $values = array()) {
    $values += (array) self::defaultEntity();
    return (object) $values;
  }

  /**
   * TODO: Complete docblock.
   */
  public function delete($ids) {
    if (!empty($ids)) {
      $transaction = db_transaction();
      $entities = $this->load($ids);

      try {
        foreach ($entities as $id => $entity) {
          module_invoke_all('entity_delete', $entity, 'agreement_entity');
          field_attach_delete('agreement_entity', $entity);
        }

        db_delete('agreement')->condition('agreement_id', $ids, 'IN')->execute();
        db_delete('agreement_user_status')->condition('agreement_id', $ids, 'IN')->execute();
        $this->resetCache();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('agreement', $e);
        throw $e;
      }
    }

    return FALSE;
  }

  /**
   * TODO: Complete docblock.
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = parent::load($ids, $conditions);
    if (!empty($entities)) {
      // TODO: Determine why this is needed as 'paths' and 'roles' are
      // serialized types.
      foreach ($entities as $k => &$entity) {
        if (is_string($entity->paths) && !empty($entity->paths)) {
          $paths = @unserialize($entity->paths);
          $entity->paths = $paths ? $paths : array();
        }

        if (is_string($entity->roles) && !empty($entity->roles)) {
          $roles = @unserialize($entity->roles);
          $entity->roles = $roles ? $roles : array();
        }
      }
    }

    return $entities;
  }

  /**
   * TODO: Complete docblock.
   */
  public function save($entity) {
    $transaction = db_transaction();
    $saved = FALSE;
    try {
      if (!empty($entity->agreement_id) && !isset($entity->original)) {
        $entity->original = entity_load_unchanged('agreement_entity', $entity->agreement_id);
      }

      field_attach_presave('agreement_entity', $entity);

      if (!isset($entity->is_new)) {
        $entity->is_new = empty($entity->agreement_id);
      }

      if (empty($entity->created)) {
        $entity->created = REQUEST_TIME;
      }

      $entity->updated = REQUEST_TIME;

      module_invoke_all('entity_presave', $entity, 'agreement_entity');

      if ($entity->is_new) {
        $saved = drupal_write_record('agreement', $entity);
        $op = 'insert';
      }
      else {
        $saved = drupal_write_record('agreement', $entity, 'agreement_id');
        $op = 'update';
      }

      $function = 'field_attach_' . $op;
      $function('agreement_entity', $entity);

      unset($entity->is_new);
      unset($entity->original);

      $this->resetCache(array($entity->agreement_id));

      db_ignore_slave();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('agreement_entity', $e);
      throw $e;
    }

    return $saved;
  }

  /**
   * TODO: Complete docblock.
   */
  public function invoke($hook, $entity) {
    if (isset($this->entityInfo['fieldable']) && function_exists($function = 'field_attach_' . $hook)) {
      $function($this->entityType, $entity);
    }

    if (module_exists('rules')) {
      rules_invoke_all($this->entityType . '_' . $hook, $entity);
    }
    else {
      module_invoke_all($this->entityType . '_' . $hook, $entity);
    }

    if ('presave' == $hook || 'insert' == $hook || 'update' == $hook || 'delete' == $hook) {
      module_invoke_all('entity_' . $hook, $entity, $this->entityType);
    }
  }

  /**
   * Not implemented.
   */
  public function view($entities, $view_mode = 'full', $langcode = NULL, $page = NULL) {
    // Empty.
  }

  /**
   * Not implemented.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL) {
    // Empty.
  }

  /**
   * Not implemented.
   */
  public function export($entity, $prefix = '') {
    throw new Exception(__METHOD__ . ' is not implemented.');
  }

  /**
   * Not implemented.
   */
  public function import($export) {
    throw new Exception(__METHOD__ . ' is not implemented.');
  }
}
