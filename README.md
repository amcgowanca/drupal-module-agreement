# Drupal Module: Agreement
--------------------------

This is a heavily adapted and modified version, version "3.0" perhaps, of the agreement module available at [drupal.org/project/agreement](http://drupal.org/project/agreement).

## TODO
Below is a list of items to complete for this module:

* AgreementEntityController: Implement the `view()` and `buildContent()` methods.
* Views Integration: The initial 3.x has removed the integration with Views.

## License

For license information, please review the License text file included within this package.
